﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Furion.ObjectMapper;
using Mapster;
using YBlog.Application.Contracts.System.Menus;
using YBlog.Core.System;

namespace YBlog.Application
{
    public class SystemMapperProfile : IObjectMapper
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<MenuDto, Route>()
                .NameMatchingStrategy(NameMatchingStrategy.ConvertSourceMemberName(n => n.Replace("Route", "")));
        }
    }
}
