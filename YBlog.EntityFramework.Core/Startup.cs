﻿using Furion;
using Furion.DatabaseAccessor;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using YBlog.EntityFramework.Core.FreeSqlUtils;

namespace YBlog.EntityFramework.Core
{
    public class Startup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(FreeSqlHelper.Build());
            services.AddDatabaseAccessor(options =>
            {
                options.AddDbPool<DefaultDbContext>($"{DbProvider.MySql}@8.0.20");
            }, "YBlog.Database.Migrations");
        }

    }
}