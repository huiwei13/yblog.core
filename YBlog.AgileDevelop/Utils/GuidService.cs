﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YBlog.Application;

namespace YBlog.AgileDevelop.Utils
{
    [ApiDescriptionSettings("AgileDevelop")]
    public class GuidService: IBaseService
    {
        public string GetAsync()
        {
            return FreeUtil.NewMongodbId().ToString();
        }
    }
}
