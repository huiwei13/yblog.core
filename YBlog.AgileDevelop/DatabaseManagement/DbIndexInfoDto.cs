﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YBlog.AgileDevelop.DatabaseManagement
{
    public class DbIndexInfoDto
    {
        public string Name { get; set; }
        public bool IsUnique { get; set; }
    }
}
