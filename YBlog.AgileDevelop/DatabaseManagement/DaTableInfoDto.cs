﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreeSql.DatabaseModel;

namespace YBlog.AgileDevelop.DatabaseManagement
{
    public class DaTableInfoDto
    {
        public string Id { get; set; }
        public string Schema { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public DbTableType Type { get; set; }
        public List<DbColumnInfoDto> Columns { get; set; }
        public List<DbIndexInfoDto> Uniques { get; }
        public List<DbIndexInfoDto> Indexes { get; }
        public List<DbForeignInfoDto> Foreigns { get; }
    }
}
