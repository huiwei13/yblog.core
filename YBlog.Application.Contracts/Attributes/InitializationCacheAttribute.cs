﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YBlog.Application.Contracts
{
    /// <summary>
    /// 初始化缓存
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class InitializationCacheAttribute: Attribute
    {
    }
}
