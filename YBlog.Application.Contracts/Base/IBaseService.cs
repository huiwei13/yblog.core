﻿using Furion.DependencyInjection;
using Furion.DynamicApiController;

namespace YBlog.Application
{
    public interface IBaseService: IScoped, IDynamicApiController
    {
    }
}
