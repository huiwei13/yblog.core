﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using YBlog.Core.System;

namespace YBlog.Application.Contracts.System.Menus
{
    public interface IMenuService
    {
        /// <summary>
        /// 创建菜单
        /// </summary>
        /// <param name="createUpdateDto"></param>
        /// <returns></returns>
        Task<object> CreateAsync(MenuCreateUpdateDto createUpdateDto);
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="createUpdateDto"></param>
        /// <returns></returns>

        Task<object> UpdateAsync(MenuCreateUpdateDto createUpdateDto);
        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteAsync(string id);
        /// <summary>
        /// 获取全部菜单
        /// </summary>
        /// <returns></returns>
        Task<List<MenuDto>> GetListAsync(MenuSearchDto searchDto);
        /// <summary>
        /// 获取全部
        /// </summary>
        /// <returns></returns>
        Task<List<MenuDto>> GetAllAsync();
        /// <summary>
        /// 根据ID获取菜单
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        Task<MenuDto> GetAsync(string keyValue);
        /// <summary>
        /// 根据父Id获取子菜单
        /// </summary>
        /// <returns></returns>
        Task<List<MenuDto>> GetChildrenAsync(string parentId);
    }
}
