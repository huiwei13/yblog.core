﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Routing;
using YBlog.Application.Contracts.System.Routes;
using YBlog.Core;
using YBlog.Core.System;

namespace YBlog.Application.Contracts.System.Menus
{
    public class MenuCreateUpdateDto
    {
        /// <summary>
        /// 菜单ID
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [MaxLength(32)]
        public string Name { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        [MaxLength(32)]
        public string Icon { get; set; }
        /// <summary>
        /// 菜单类型
        /// </summary>
        public MenuType? Type { get; set; }
        /// <summary>
        /// 父ID
        /// </summary>
        public string ParentId { get; set; }
        /// <summary>
        /// 是否有子项
        /// </summary>
        public bool HasChild { get; set; }

        /// <summary>
        /// 路由ID
        /// </summary>
        public string RouteId { get; set; }
        /// <summary>
        /// 路由名称
        /// </summary>
        public string RouteName { get; set; }
        /// <summary>
        /// 路由路径
        /// </summary>
        public string RoutePath { get; set; }
        /// <summary>
        /// 重定向
        /// </summary>
        public string RouteRedirect { get; set; }
        /// <summary>
        /// 路由组件
        /// </summary>
        public string RouteComponent { get; set; }
    }
}
