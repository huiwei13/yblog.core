﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YBlog.Application.Contracts.System.DataItems
{
    /// <summary>
    /// 数据项分类
    /// </summary>
    public class DataItemCategoryDto: TreeModel<DataItemCategoryDto>
    {
        /// <summary>
        /// 分类名称
        /// </summary>
        public string Name { get; set; }
    }
}
