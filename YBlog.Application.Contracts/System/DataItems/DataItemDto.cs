﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YBlog.Core.System;

namespace YBlog.Application.Contracts.System.DataItems
{
    /// <summary>
    /// 数据项
    /// </summary>
    public class DataItemDto:BaseEntityDto
    {
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public DataItemType Type { get; set; }
        /// <summary>
        /// 分类ID
        /// </summary>
        public string DataItemCategoryId { get; set; }
    }
}
