﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YBlog.Core.System;

namespace YBlog.Application.Contracts.System.DataItems
{
    /// <summary>
    /// 创建或更新
    /// </summary>
    public class DataItemCreateUpdateDto: DataItemDto
    {
        /// <summary>
        /// 数据项明细
        /// </summary>
        public List<DataItemDetailDto> Details { get; set; }
    }
}
