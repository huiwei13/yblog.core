﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YBlog.Application.Contracts
{
    /// <summary>
    /// 联级Dto
    /// </summary>
    public class CascaderDto
    {
        /// <summary> 
        /// 名称
        /// </summary>
        public string Label { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// 是否有子集
        /// </summary>
        public bool HasChild { get; set; }
        /// <summary>
        /// 下级
        /// </summary>
        public List<CascaderDto> Children { get; set; }
    }
}
