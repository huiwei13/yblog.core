﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Primitives;

namespace YBlog.Application
{
    public interface IMemoryCacheService
    {
        /// <summary>
        /// key值
        /// </summary>
        static HashSet<string> Keys { get; }
        /// <summary>
        /// 设置
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        TItem Set<TItem>(string key, TItem value);
        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        TItem Get<TItem>(string key);

        /// <summary>
        /// 获取或创建
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="key"></param>
        /// <param name="factory"></param>
        /// <returns></returns>
        Task<TItem> GetOrCreateAsync<TItem>(string key, Func<ICacheEntry, Task<TItem>> factory);
        /// <summary>
        /// 删除内存缓存
        /// </summary>
        /// <param name="key"></param>
        Task RemoveAsync(string key);
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="key"></param>
        void Update(string key);
        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <returns></returns>
        Task Clear();
    }
}
