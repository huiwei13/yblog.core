﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YBlog.Application.Contracts.System.Routes
{
    public interface IRouteService
    {
        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        Task<List<RouteDto>> GetListAsync(Search search);
        /// <summary>
        /// 查询单个
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        Task<RouteDto> GetByPathAsync(string path);
        /// <summary>
        /// 获取可访问路由
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<List<RouteDto>> GetAccessListAsync(string token);
    }
}
