﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YBlog.Core.System;

namespace YBlog.Application.Contracts.System.Routes
{
    public class RouteDto:BaseEntityDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// 重定向
        /// </summary>
        public string Redirect { get; set; }
        /// <summary>
        /// 组件
        /// </summary>
        public string Component { get; set; }
        /// <summary>
        /// 保持生机（缓存标志）
        /// </summary>
        public bool KeepAlive { get; set; }
        /// <summary>
        /// 子路由
        /// </summary>
        public List<RouteDto> Children { get; set; }
    }
}
