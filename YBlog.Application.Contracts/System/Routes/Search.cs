﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YBlog.Application.Contracts.System.Routes
{
    public class Search
    {
        public string Keyword { get; set; }
    }
}
