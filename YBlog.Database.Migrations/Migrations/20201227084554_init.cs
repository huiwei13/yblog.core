﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace YBlog.Database.Migrations.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Sys_DataItem",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false),
                    EnCode = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Type = table.Column<int>(type: "int", nullable: false),
                    CreateUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    CreateTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    IsEnabled = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    DeleteUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    DeleteTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdateUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    UpdateTime = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_DataItem", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sys_DataItemDetail",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false),
                    DataItemId = table.Column<Guid>(type: "char(36)", nullable: false),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Value = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    ShortName = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    CreateUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    CreateTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    IsEnabled = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    DeleteUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    DeleteTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdateUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    UpdateTime = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_DataItemDetail", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sys_Menu",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false),
                    Name = table.Column<string>(type: "varchar(32) CHARACTER SET utf8mb4", maxLength: 32, nullable: true),
                    Icon = table.Column<string>(type: "varchar(32) CHARACTER SET utf8mb4", maxLength: 32, nullable: true),
                    Type = table.Column<int>(type: "int", nullable: true),
                    ParentId = table.Column<Guid>(type: "char(36)", nullable: true),
                    RouteId = table.Column<Guid>(type: "char(36)", nullable: true),
                    CreateUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    CreateTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    IsEnabled = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    DeleteUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    DeleteTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdateUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    UpdateTime = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_Menu", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sys_Resource",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Extension = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Path = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Type = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Size = table.Column<long>(type: "bigint", nullable: false),
                    ResourceDirectoryId = table.Column<Guid>(type: "char(36)", nullable: true),
                    CreateUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    CreateTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    IsEnabled = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    DeleteUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    DeleteTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdateUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    UpdateTime = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_Resource", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sys_ResourceDirectory",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Path = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    IsPublic = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    CreateUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    CreateTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    IsEnabled = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    DeleteUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    DeleteTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdateUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    UpdateTime = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_ResourceDirectory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sys_Route",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false),
                    Name = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Path = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Redirect = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Component = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    CreateUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    CreateTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    IsEnabled = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    DeleteUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    DeleteTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdateUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    UpdateTime = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_Route", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sys_User",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false),
                    Account = table.Column<string>(type: "varchar(32) CHARACTER SET utf8mb4", maxLength: 32, nullable: true),
                    RealName = table.Column<string>(type: "varchar(32) CHARACTER SET utf8mb4", maxLength: 32, nullable: true),
                    NickName = table.Column<string>(type: "varchar(32) CHARACTER SET utf8mb4", maxLength: 32, nullable: true),
                    HeadIcon = table.Column<string>(type: "varchar(100) CHARACTER SET utf8mb4", maxLength: 100, nullable: true),
                    Gender = table.Column<int>(type: "int", nullable: false),
                    Birthday = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    MobilePhone = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Email = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    WeChat = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    QQ = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Country = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    Province = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    City = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    District = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
                    CreateUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    CreateTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    IsEnabled = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    IsDeleted = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    DeleteUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    DeleteTime = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdateUserId = table.Column<Guid>(type: "char(36)", nullable: true),
                    UpdateTime = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sys_User", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Sys_Menu",
                columns: new[] { "Id", "CreateTime", "CreateUserId", "DeleteTime", "DeleteUserId", "Icon", "IsDeleted", "IsEnabled", "Name", "ParentId", "RouteId", "Type", "UpdateTime", "UpdateUserId" },
                values: new object[,]
                {
                    { new Guid("e92887cb-b677-cff3-ef1a-1726a9e23898"), null, null, null, null, null, false, true, "系统管理", null, null, 0, null, null },
                    { new Guid("00970789-4807-2b81-8c76-297a3378120e"), null, null, null, null, null, false, true, "功能", new Guid("e92887cb-b677-cff3-ef1a-1726a9e23898"), null, 1, null, null },
                    { new Guid("7e5ed5a2-9f9a-eb69-8689-6704e254754c"), null, null, null, null, null, false, true, "菜单", new Guid("00970789-4807-2b81-8c76-297a3378120e"), null, 2, null, null },
                    { new Guid("457ae372-6ca2-f666-dbc5-569d7583495a"), null, null, null, null, null, false, true, "示例", null, null, 0, null, null },
                    { new Guid("d429a179-fa19-5ce3-ba8c-4e05dadcba4e"), null, null, null, null, null, false, true, "界面", new Guid("457ae372-6ca2-f666-dbc5-569d7583495a"), null, 1, null, null },
                    { new Guid("0908543d-df4c-561e-3d48-26f3264d9cbd"), null, null, null, null, null, false, true, "查询", new Guid("d429a179-fa19-5ce3-ba8c-4e05dadcba4e"), null, 2, null, null },
                    { new Guid("a601efc0-5779-fbad-3c19-c98c6cd6891d"), null, null, null, null, null, false, true, "新增", new Guid("d429a179-fa19-5ce3-ba8c-4e05dadcba4e"), null, 2, null, null },
                    { new Guid("5ad1066d-1478-146d-5c8f-8f39d5f676bd"), null, null, null, null, null, false, true, "编辑", new Guid("d429a179-fa19-5ce3-ba8c-4e05dadcba4e"), null, 2, null, null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Sys_DataItem");

            migrationBuilder.DropTable(
                name: "Sys_DataItemDetail");

            migrationBuilder.DropTable(
                name: "Sys_Menu");

            migrationBuilder.DropTable(
                name: "Sys_Resource");

            migrationBuilder.DropTable(
                name: "Sys_ResourceDirectory");

            migrationBuilder.DropTable(
                name: "Sys_Route");

            migrationBuilder.DropTable(
                name: "Sys_User");
        }
    }
}
