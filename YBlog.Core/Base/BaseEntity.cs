﻿using System;
using System.ComponentModel.DataAnnotations;
using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Newtonsoft.Json;

namespace YBlog.Core
{
    [SkipScan]
    public class BaseEntity : IBaseEntity, ICreateAduitEntity, IDeleteAduitEntity, IUpdateAuditEntity
    {
        /// <summary>
        /// 实体主键
        /// </summary>
        [Key]
        [MaxLength(36)]
        public string Id { get; set; } = FreeUtil.NewMongodbId().ToString();
        /// <summary>
        /// 创建用户主键
        /// </summary>
        public Guid? CreateUserId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsEnabled { get; set; } = true;
        /// <summary>
        /// 是否删除
        /// </summary>
        [FakeDelete(true)]
        public bool IsDeleted { get; set; } = false;
        /// <summary>
        /// 删除用户主键
        /// </summary>
        public Guid? DeleteUserId { get; set; }
        /// <summary>
        /// 删除时间
        /// </summary>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 修改用户主键
        /// </summary>
        public Guid? UpdateUserId { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
    }
}
