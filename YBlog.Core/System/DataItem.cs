﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace YBlog.Core.System
{
    /// <summary>
    /// 数据项分类
    /// </summary>
    [Table("Sys_DataItemCategory")]
    public class DataItemCategory : BaseEntity,IEntitySeedData<DataItemCategory>
    {
        /// <summary>
        /// 分类名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 父级Id
        /// 最顶级的父Id为 '0'
        /// </summary>
        public string ParentId { get; set; }
        /// <summary>
        /// 左节点
        /// </summary>
        public int Lft { get; set; }
        /// <summary>
        /// 右节点
        /// </summary>
        public int Rgt { get; set; }
        /// <summary>
        /// 种植数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<DataItemCategory> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new List<DataItemCategory> 
            {
                new DataItemCategory{ Id = "5ff4dd74-2999-3150-0090-c54313cdb591", Name = "系统管理", ParentId = "0", Lft = 0, Rgt = 5 },
                new DataItemCategory{ Id = "5ff4ddc4-2999-3150-0090-c54458c0c811", Name = "功能", ParentId = "5ff4dd74-2999-3150-0090-c54313cdb591", Lft = 1, Rgt = 2 },
                new DataItemCategory{ Id = "5ff4de5d-2999-3150-0090-c545721b25f4", Name = "设置", ParentId = "5ff4dd74-2999-3150-0090-c54313cdb591", Lft = 3, Rgt = 4 },
            };
        }
    }
    /// <summary>
    /// 数据项
    /// </summary>
    [Table("Sys_DataItem")]
    public class DataItem : BaseEntity,IEntitySeedData<DataItem>
    {
        /// <summary>
        /// 分类ID
        /// </summary>
        public string DataItemCategoryId { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public DataItemType Type { get; set; }

        public IEnumerable<DataItem> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new List<DataItem>
            {
                new DataItem{ DataItemCategoryId = "5ff4ddc4-2999-3150-0090-c54458c0c811",Id = "5ff4ded3-2999-3150-0090-c54609c792e1",Code = "MenuType",Name = "菜单类型",Type = DataItemType.Dict },
                new DataItem{ DataItemCategoryId = "5ff4ddc4-2999-3150-0090-c54458c0c811",Id = "5ff4df02-2999-3150-0090-c54743ae8369",Code = "DataItemType",Name = "数据项类型",Type = DataItemType.Dict },
            };
        }
    }

    /// <summary>
    /// 数据项明细
    /// </summary>
    [Table("Sys_DataItemDetail")]
    public class DataItemDetail : BaseEntity,IEntitySeedData<DataItemDetail>
    {
        /// <summary>
        /// 数据项Id
        /// </summary>
        public string DataItemId { get; set; }
        /// <summary>
        /// 显式名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 隐式值
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// 简称
        /// </summary>
        public string ShortName { get; set; }

        public IEnumerable<DataItemDetail> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new List<DataItemDetail>
            {
                // 菜单类型
                new DataItemDetail{ Id = "5ff4df67-2999-3150-0090-c5482ae88786",DataItemId = "5ff4ded3-2999-3150-0090-c54609c792e1",Name = "根菜单",Value = "0",ShortName = "GCD" },
                new DataItemDetail{ Id = "5ff4df71-2999-3150-0090-c5491c774728",DataItemId = "5ff4ded3-2999-3150-0090-c54609c792e1",Name = "二级菜单",Value = "1",ShortName = "EJCD" },
                new DataItemDetail{ Id = "5ff4dff2-2999-3150-0090-c54c769c38b5",DataItemId = "5ff4ded3-2999-3150-0090-c54609c792e1",Name = "视图菜单",Value = "2",ShortName = "STCD" },
                // 数据项类型
                new DataItemDetail{ Id = "5ff4df78-2999-3150-0090-c54a3d341ff1",DataItemId = "5ff4df02-2999-3150-0090-c54743ae8369",Name = "字典",Value = "0",ShortName = "ZD" },
                new DataItemDetail{ Id = "5ff4df7f-2999-3150-0090-c54b3b02b4e0",DataItemId = "5ff4df02-2999-3150-0090-c54743ae8369",Name = "数据源",Value = "1",ShortName = "SJY" },
            };
        }
    }
    /// <summary>
    /// 数据项类型
    /// </summary>
    public enum DataItemType
    { 
        Dict,Source
    }
}
