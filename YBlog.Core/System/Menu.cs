﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace YBlog.Core.System
{
    [Table("Sys_Menu")]
    public class Menu: BaseEntity, IEntitySeedData<Menu>, IEntityTypeBuilder<Menu>
    {
        /// <summary>
        /// 名称
        /// </summary>
        [MaxLength(32)]
        public string Name { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        [MaxLength(32)]
        public string Icon { get; set; }
        /// <summary>
        /// 菜单类型
        /// Root 根菜单
        /// Group 二级菜单、这里是作为分组
        /// View 视图菜单
        /// </summary>
        public MenuType? Type { get; set; }
        /// <summary>
        /// 父级Id
        /// 最顶级的父Id为 '0'
        /// </summary>
        public string ParentId { get; set; }
        /// <summary>
        /// 路由外键
        /// </summary>
        public string RouteId { get; set; }

        public void Configure(EntityTypeBuilder<Menu> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
        }

        public IEnumerable<Menu> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new List<Menu>
            {
                new Menu{ Name = "系统管理",Type = MenuType.Root ,Id = "5fefa999-e659-9c58-005c-4f70414fd40c",ParentId = "0" },
                new Menu{ Name = "功能",Type = MenuType.Group ,Id = "5fefa9e1-e659-9c58-005c-4f715d544b6f",ParentId = "5fefa999-e659-9c58-005c-4f70414fd40c"},
                new Menu{ Name = "菜单",Type = MenuType.View,Id = "5fefa9f5-e659-9c58-005c-4f7275f0545e" ,ParentId = "5fefa9e1-e659-9c58-005c-4f715d544b6f",RouteId = "5ff05630-b33d-a7c8-0014-effb6f4d89ab"},


                new Menu{ Name = "示例",Type = MenuType.Root,Id = "5ff0d897-b216-a6cc-0000-177979a1dda4",ParentId = "0" ,RouteId = "5ff0c7fd-c953-f494-00b4-329b51a095c2"},
            };
        }
    }
}
