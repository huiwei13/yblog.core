﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Furion.DatabaseAccessor;

namespace YBlog.Core.System
{
    [Table("Sys_User")]
    public class User:BaseEntity
    {
        /// <summary>
        /// 账户
        /// </summary>
        [MaxLength(32)]
        public string Account { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        [MaxLength(32)]
        public string RealName { get; set; }

        /// <summary>
        /// 呢称
        /// </summary>
        [MaxLength(32)]
        public string NickName { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        [MaxLength(100)]
        public string HeadIcon { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 微信
        /// </summary>
        public string WeChat { get; set; }
        /// <summary>
        /// QQ
        /// </summary>
        public string QQ { get; set; }

        /// <summary>
        /// 国家
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// 省份
        /// </summary>
        public string Province { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 地区
        /// </summary>
        public string District { get; set; }
    }

    public enum Gender
    {
        /// <summary>
        /// 男性
        /// </summary>
        Male, 
        /// <summary>
        /// 女性
        /// </summary>
        Female, 
        /// <summary>
        /// renyao 
        /// </summary>
        Shemale
    }
}
