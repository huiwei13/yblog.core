﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YBlog.Core.System
{
    /// <summary>
    /// 资源目录
    /// </summary>
    [Table("Sys_ResourceDirectory")]
    public class ResourceDirectory : BaseEntity 
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// 公开
        /// </summary>
        public bool IsPublic { get; set; }
    }
    /// <summary>
    /// 资源
    /// </summary>
    [Table("Sys_Resource")]
    public class Resource : BaseEntity
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 扩展名
        /// </summary>
        public string Extension { get; set; }
        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 大小
        /// </summary>
        public long Size { get; set; }
        /// <summary>
        /// 目录Id
        /// </summary>
        public Guid? ResourceDirectoryId { get; set; }

    }
}
