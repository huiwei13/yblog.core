﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace YBlog.Core.System
{
    /// <summary>
    /// 路由
    /// </summary>
    [Table("Sys_Route")]
    public class Route : BaseEntity, IEntitySeedData<Route>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// 重定向
        /// </summary>
        public string Redirect { get; set; }
        /// <summary>
        /// 组件
        /// </summary>
        public string Component { get; set; }
        /// <summary>
        /// 保持生机（缓存标志）
        /// </summary>
        public bool KeepAlive { get; set; }

        public IEnumerable<Route> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new List<Route> 
            {
                #region Home
                new Route{ Id = "0",Name = "Home",Path = "/",Redirect = "desktop",Component = "Home",KeepAlive = true },
	            #endregion

                #region 桌面
                new Route{ Id = "5ff0c7ee-c953-f494-00b4-329a422c0eee",Name = "桌面",Path = "/desktop",Component = "system/desktop",KeepAlive = true },
	            #endregion

                #region 菜单
                new Route{ Id = "5ff05630-b33d-a7c8-0014-effb6f4d89ab",Name = "菜单",Path = "/menu",Component = "system/menu/index",KeepAlive = true },
                new Route{ Id = "5ff0c60b-7e5e-4fc8-009b-23a92e481348",Name = "菜单Form",Path = "/menu/form",Component = "system/menu/form",KeepAlive = false },
                #endregion
                
                #region 数据管理
                new Route{ Id = "5ff23e0e-8f52-f338-00b5-894b4613aac7",Name = "数据",Path = "/data",Component = "system/data/index",KeepAlive = true },
                new Route{ Id = "5ff23e2c-8f52-f338-00b5-894c7c07e767",Name = "数据Form",Path = "/data/form",Component = "system/data/form",KeepAlive = false },
	            #endregion

                #region 示例
                new Route{ Id = "5ff0c7fd-c953-f494-00b4-329b51a095c2",Name = "示例",Path = "/sample", Component = "sample/index",KeepAlive = true },
                new Route{ Id = "5ff0c80d-c953-f494-00b4-329c6acae8cf",Name = "示例Form",Path = "/sample/form",Component = "system/form",KeepAlive = false },

	            #endregion

            };
        }
    }
}
